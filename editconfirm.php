<?php
session_start();
$servername = "localhost";
$database = "teachers";
$username = "root";
$password = "";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

mysqli_set_charset($conn, "utf8");

   
    if (isset($_POST['checkedit'])) {
        // Lấy dữ liệu người dùng hiệu chỉnh gởi từ REQUEST POST
        $id = $_SESSION['id'];
        $usernamecheck = $_SESSION['username'];
        $majors = $_SESSION['majors'];
        $degree = $_SESSION['degree'];
        $describe = $_SESSION['describe'];
        $avatar = $_SESSION['avatar'];

        
        // Câu lệnh UPDATE
        $sql = "UPDATE teachers SET name ='$usernamecheck', specialized ='$majors', degree ='$degree', description = '$describe', avatar ='$avatar' WHERE id = '$id';";

        // Thực thi UPDATE
        if(mysqli_query($conn, $sql)){
            header("Location: complete_regist.php");
            
        }
        else{
        echo "cập nhật thất bại";
        }
        

        // Đóng kết nối
        mysqli_close($conn);

        // Sau khi cập nhật dữ liệu, tự động điều hướng về trang Danh sách
        
    }
?>

<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <!-- CSS only -->
    
    <title>Document</title>
    
    <style>
        .containerbody {
            width: 600px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            
        }

        .containerbody-margin {
        
            margin-left: 55px;
        }

        .label{
            color: red;
        }
        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;
            border-collapse:initial;
        }

        /* input[type='date']::-webkit-inner-spin-button,
        input[type='date']::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
} */

        .display {
            width: 82px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #000;
        }
        
        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 18px;
            background-color: blue;
            
            
            
        }

        /* .error{
            display: block;
        } */

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        
        /* .datetime{
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
        } */

        .selectbox {
            width: 250px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }
        
        #signup a{text-decoration: none}

        #signup {
            margin-left: 15%;
            margin-top: 25px;
            
            

        }
        .custombrowse{
            color: white;
            background-color:blue;
            height: 20px;
            width: 48px;
            border: #006ccb;
            
            font-size: 15px;
            border: solid #006ccb 2px;
            margin-left: 5px;
            padding: 8px 15px;

        }
        .customsignup {
            color: white;
            background-color:blue;
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
            border: solid #006ccb 2px;
            margin-bottom: 10px;
            padding: 12px 40px;
            margin-right: 100px;
        }
        .next {
            color: white;
            background-color:blue;
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
            border: solid #006ccb 2px;
            margin-bottom: 10px;
            padding: 10px 22px;
        }
    </style>
</head>

<body>

    <div class='containerbody'>
    <form action='' method='POST' enctype = 'multipart/form-data'>
        <div class='containerbody-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>
            
            
            <table class='custom-table' >

                
                <tr>
                    
                    <td >
                    
                        <label for="" >Họ và tên</label>  
                    </td>
                    
                    <td><input class="display2" name="username" type="text" maxlength="100" value = '<?php echo  $_SESSION['username'] ?>' readonly></td>
                    
                    
                </tr>
                
                
                <tr>
                    <td class='' > 
                        <label for="">Chuyên nghành</label> 
                    </td>
                    
                    <td>
                    <?php
                            $chuyennganh = array("" => "", "001" => "Khoa học máy tính", "002" => "Khoa học dữ liệu", "003" => "Hải dương học");
                            foreach ($chuyennganh as $key => $value) {
                                if($_SESSION['majors'] == $key){
                                    echo "
                                    <input class='display2' name='degree' type='text'  readonly ";
                                    echo "  value='" . $value . "'>";
                                    }
                                    
                                }
                               
                                
                            ?>
                    </td> 
                    </td>
                        
                </tr>
                
                
                <tr>
                    <td class='' > 
                        <label for="">Bằng cấp</label>  
                    </td>
                    <td>
                        <?php
                            $bangcap = array("" => "", "001" => "Cử nhân", "002" => "Thạc sĩ", "003" => "Tiến sĩ", "004" => "Phó giáo sư", "005" => "Giáo sư");
                            foreach ($bangcap as $key => $value) {
                                if($_SESSION['degree'] == $key){
                                    echo "
                                    <input class='display2' name='degree' type='text'  readonly ";
                                    echo "  value='" . $value . "'>";
                                    }
                                    
                                }
                               
                                
                            ?>
                    </td>   
                        
                </tr>

                

                
                <tr>
                    <td class='' >
                        <label for="">Avatar</label> 
                    </td>
                        <td> 
                        <div >
                            <img  src = '<?php echo  $_SESSION['hinhanh'] ?>' width = '100' height = '90'>
                            
                        </div> 
                        
                        </td>
                    </tr>  
                    <!-- <tr>
                        
                    </tr> -->
                    
                
                
                <tr>
                    <td class='' >
                        <label for="">Mô tả thêm</label> 
                    </td>
                    <td>
                        <textarea  name="describe" rows="7" cols="50"  readonly><?php echo $_SESSION['describe'] ?></textarea> 
                    </td>
                </tr>

                

            </table>

            <div id='signup'>
                <a class='customsignup' href=" edit.php?id=<?php echo $_SESSION['id']; ?>" > Sửa lại</a>
                <input class='next' type='submit' name = 'checkedit' value='Xác nhận'>
            </div>
            <div id='signup'>
                
                
                
        </div>
            </form>
        </div>
</body>

</html>