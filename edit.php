<!DOCTYPE html>
<html lang='en'>

<?php
session_start();
$servername = "localhost";
$database = "teachers";
$username = "root";
$password = "";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

mysqli_set_charset($conn, "utf8");
$id = $_GET['id'];

$sql = "SELECT * FROM teachers where id = '$id'";
$result = mysqli_query($conn, $sql);

while($row = mysqli_fetch_assoc($result)) {

    if (empty($_SESSION['id']) || $id != $_SESSION['id']) {
        $_SESSION['username'] = $row["name"];
        $_SESSION['majors'] = $row["specialized"];
        $_SESSION['degree'] = $row["degree"];
        $_SESSION['describe'] = $row["description"];
        $_SESSION['avatar'] = $row["avatar"];
        }
    }


if ($_SERVER['REQUEST_METHOD'] == "POST") 
{
     
    
    $error = array();   
    $_SESSION = $_POST; 
    $_SESSION['id'] = $id;
    $allowed_exttension = array('gif', 'png', 'jpg','jpeg','jfif');
    $hinhanh = basename($_FILES['avatar']['name']);
    $target_dir = "web/";
                        
    if(!file_exists($target_dir)){
           mkdir($target_dir);
    }
    if(!empty($hinhanh)){
        
        $file_exttension = pathinfo( $hinhanh, PATHINFO_EXTENSION);
       
        $target_file = $target_dir .  $hinhanh;
        if(in_array($file_exttension, $allowed_exttension) && empty($error)== true){
            (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file));
        }
        $_SESSION['avatar'] = $hinhanh;
    }
    else{
        $file_exttension = pathinfo( $_POST['avatar'], PATHINFO_EXTENSION);
        
        $target_file = $target_dir .  $_SESSION['avatar'];  
    }
    
    $_SESSION['hinhanh'] = $target_file;

//Các hàm check lỗi,khi chưa nhập sẽ báo lỗi
    if(empty($_POST['username'])) {
        $error['username'] = "Hãy nhập tên giáo viên";
    }else{
        $username = $_POST['username'];
    }

    if(empty($_POST['majors'])) {
        $error['majors'] = "Hãy chọn bộ môn";
    }else{
        $majors = $_POST['majors'];
    }

    if(empty($_POST['degree'])) {
        $error['degree'] = "Hãy chọn bằng cấp";
    }else{
        $degree = $_POST['degree'];
    }

    if(empty($_POST['describe'])) {
        $error['describe'] = "Hãy nhập mô tả chi tiết";
    }else{
        $date = $_POST['describe'];
    }
    
    if(!in_array($file_exttension, $allowed_exttension)){
        
            $error['avatar'] = "Hãy chọn avatar";
        
    }
    
    
    if(empty($error)) {
        // mysqli_query($conn,"
        //                     insert into student (name,gender,faculty,birthday,address,avartar)
        //                     values ('$username','$gender','$khoa','$date','$address','$target_file')");
        header("Location: editconfirm.php?id=". $id ."");
    }
}

?>
    

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <!-- CSS only -->
    
    <title>Document</title>
    
    <style>
        .containerbody {
            width: 600px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            
        }

        .containerbody-margin {
        
            margin-left: 55px;
        }

        .label{
            color: red;
        }
        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;
            border-collapse:initial;
        }

        /* input[type='date']::-webkit-inner-spin-button,
        input[type='date']::-webkit-calendar-picker-indicator {
        display: none;
        -webkit-appearance: none;
} */

        .display {
            width: 82px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #000;
        }
        
        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 18px;
            background-color: blue;
            
            
            
        }

        /* .error{
            display: block;
        } */

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        
        /* .datetime{
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
        } */

        .selectbox {
            width: 250px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }
        
        #signup a{text-decoration: none}

        #signup {
            margin-left: 35%;
            margin-top: 25px;
            

        }
        .custombrowse{
            color: white;
            background-color:blue;
            height: 20px;
            width: 48px;
            border: #006ccb;
            
            font-size: 15px;
            border: solid #006ccb 2px;
            margin-left: 5px;
            padding: 8px 15px;

        }
        .customsignup {
            color: white;
            background-color:blue;
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
            border: solid #006ccb 2px;
            margin-bottom: 10px;
            padding: 10px 22px;
        }
    </style>
</head>

<body>

    <div class='containerbody'>
    <form action='' method='POST' enctype = 'multipart/form-data'>
        <div class='containerbody-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>
            
            
            <table class='custom-table' >

                <tr>
                    <td></td>
                    <td>
                    <?php
                    if (isset($error['username'])){
                        ?>
                        <label style = 'color: red; '><?php echo $error['username'] ;
                        ?>
                        </label><?php }
                        ?>
                    </td>
                
                </tr>
                <tr>
                    
                    <td >
                    
                        <label for="" >Họ và tên</label>  
                    </td>
                    
                    <td><input  class="display2" type="text" name="username"  value="<?php echo isset($_SESSION['username']) ? $_SESSION['username'] : "" ?>"></td>
                    
                    
                </tr>
                
                <tr>
                    <td></td>
                    <td>
                    <?php
                    if (isset($error['majors'])){
                        ?>
                        <label style = 'color: red; '><?php echo $error['majors'] ;
                        ?>
                        </label><?php }
                        ?>
                    </td>
                
                </tr>
                <tr>
                    <td class='' > 
                        <label for="">Chuyên nghành</label> 
                    </td>
                    
                    <td><select class='selectbox' name = 'majors' value = ''> 
                        <?php
                            $chuyennganh = array("" => "", "001" => "Khoa học máy tính", "002" => "Khoa học dữ liệu", "003" => "Hải dương học");
                            foreach ($chuyennganh as $key => $value) {
                                echo "
                                <option ";
                                echo isset($_SESSION['majors']) && $_SESSION['majors'] == $key ? "selected " : "";
                                echo "  value='" . $key . "'>" . $value  . "</option>";
                                }
                                
                                
                            ?>
                        </select></td>
                        
                </tr>
                
                <tr>
                    <td></td>
                    <td>
                    <?php
                    if (isset($error['degree'])){
                        ?>
                        <label style = 'color: red; '><?php echo $error['degree'] ;
                        ?>
                        </label><?php }
                        ?>
                    </td>
                
                </tr>
                <tr>
                    <td class='' > 
                        <label for="">Bằng cấp</label>  
                    </td>
                    <td><select class='selectbox' name = 'degree' value = ''> 
                        <?php
                            $bangcap = array("" => "", "001" => "Cử nhân", "002" => "Thạc sĩ", "003" => "Tiến sĩ", "004" => "Phó giáo sư", "005" => "Giáo sư");
                            foreach ($bangcap as $key => $value) {
                                echo "
                                <option ";
                                echo isset($_SESSION['degree']) && $_SESSION['degree'] == $key ? "selected " : "";
                                echo "  value='" . $key . "'>" . $value  . "</option>";
                                }
                                
                                
                            ?>
                        </select></td>
                        
                        
                </tr>

                

                <!-- <tr>
                    <td class='display' >Avatar <label class='label'>*</label></td>
                    <td><p></p></td>
                    

                </tr> -->
                <tr>
                    <td></td>
                    <td>
                    <?php
                    if (isset($error['avatar'])){
                        ?>
                        <label style = 'color: red; '><?php echo $error['avatar'] ;
                        ?>
                        </label><?php }
                        ?>
                    </td>
                
                </tr>
                <tr>
                    <td class='' >
                        <label for="">Avatar</label> 
                    </td>
                        <td> 
                        <div style="display: table-caption;">
                            <img id='showimg' src = '<?php echo"web/" . $_SESSION['avatar'] ?>' width = '100' height = '90'>
                            <div style="display: flex;">
                                <input class="display2" name = 'avatar'  id='showsrc' type="text" value = "<?php echo isset($_SESSION['avatar'] ) ? $_SESSION['avatar'] : "" ?>" readonly>
                                <div class="custombrowse"> 
                                <input type="file" name="avatar" id='imgInp' class="inputfile" />
                                <label for="imgInp" name="" >Browse</label>
                                
                                </div>
                            </div>
                            
                        </div> 
                        <script >
                    imgInp.onchange = evt => {
                        const [file] = imgInp.files
                        var input = event.srcElement;
                        if (file) {
                          showimg.src = URL.createObjectURL(file)
                          showsrc.value = input.files[0].name
                        }
                      }
                        </script>
                        </td>
                    </tr>  
                    <!-- <tr>
                        
                    </tr> -->
                    
                
                <tr>
                    <td></td>
                    <td>
                    <?php
                    if (isset($error['describe'])){
                        ?>
                        <label style = 'color: red; '><?php echo $error['describe'] ;
                        ?>
                        </label><?php }
                        ?>
                    </td>
                
                </tr>
                <tr>
                    <td class='' >
                        <label for="">Mô tả thêm</label> 
                    </td>
                    <td><textarea  name="describe" id="" rows="7" cols="50" maxlength="1000" ><?php echo isset($_SESSION['describe']) ? $_SESSION['describe'] : ''; ?></textarea> </td>
                </tr>

                

            </table>

            <div id='signup'>
                
                <input class='customsignup' type="submit" name = "submit" value="Xác nhận">
            </div>
           
            </form>
        </div>
</body>

</html>